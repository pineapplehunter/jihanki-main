from django.db import models
from django.contrib.auth.models import User

import datetime

# Create your models here.


class Item(models.Model):
    product_name = models.CharField(max_length=200)
    product_price = models.IntegerField()
    product_price_with_fee = models.IntegerField()
    product_Quantity = models.IntegerField(default=1)

    added_date = models.DateTimeField(null=True)

    seller = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.product_name


class Log(models.Model):
    product_name = models.CharField(max_length=200)
    product_price = models.IntegerField()
    product_price_with_fee = models.IntegerField()

    added_date = models.DateTimeField(null=True)
    deleted_date = models.DateTimeField(null=True)

    seller = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="seller_data")
    buyer = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="buyer_data")

    def __str__(self):
        return str(self.product_name) + " " + str(self.buyer)


class News(models.Model):
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class UserAndMoney(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    money = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username + " - " + self.money
