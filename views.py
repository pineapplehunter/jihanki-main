from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import ItemSerializer
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from django.contrib.auth.models import User
from .models import Item, Log, News
import math
import datetime

# Constants
DFEE = 1.1
MAX_MONEY_INPUT = 3000
SELF_FEE = True
ADD_ITEM_TO_BALANCE = False

# for user info


def get_user_info(request, context):
    if not request.user.is_authenticated:
        return context
    user = request.user
    context["userdata"] = user

    context["money"] = get_user_balance(user)
    return context


def get_user_balance(user):
    money = 0

    for log in Log.objects.filter(seller=user):
        money += log.product_price

    for log in Log.objects.filter(buyer=user):
        money -= log.product_price_with_fee

    """
    for log in Log.objects.all():
        if log.seller == user:
            money += log.product_price
        if log.buyer == user:
            money -= log.product_price_with_fee
    """

    if ADD_ITEM_TO_BALANCE:
        for item in Item.objects.all():
            if item.seller == user:
                money += item.product_price * item.product_Quantity

    return money


def render_with_userdata(request, template_name, context={}):
    context = get_user_info(request, context)
    return render(request, template_name, context=context)


# Create your views here. ************************************************
def index(request):
    return render_with_userdata(request, "jihanki/index.html", {})


class BuyView(View):
    template_name = "jihanki/buy.html"

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect("xdmain:login")
        else:
            items = Item.objects.all()
            return render_with_userdata(request, self.template_name, context={"items": items, "SELF_FEE": SELF_FEE})


class BuyConfirmView(View):
    template_name = "jihanki/buy_confirm.html"

    def get(self, request, pk):
        item = get_object_or_404(Item, pk=pk)
        return render_with_userdata(request, self.template_name, context={"item": item, "SELF_FEE": SELF_FEE})

    def post(self, request, pk):
        try:
            item = Item.objects.get(pk=pk)

            if item.product_price == 0.0:
                item.delete()
                items = Item.objects.all()
                context = {"items": items, "SELF_FEE": SELF_FEE, "OK": "OK"}
                return render_with_userdata(request, self.template_name, context=context)

            if int(item.product_price) > get_user_balance(request.user):
                item = get_object_or_404(Item, pk=pk)
                return render_with_userdata(request, self.template_name, context={"item": item, "SELF_FEE": SELF_FEE, "error": "お金が足りません"})

            log = Log()
            log.product_name = item.product_name
            log.seller = item.seller
            log.buyer = request.user
            log.added_date = item.added_date
            log.deleted_date = datetime.datetime.now()

            if SELF_FEE:
                log.product_price = item.product_price
                log.product_price_with_fee = item.product_price_with_fee
            else:
                if log.seller == log.buyer:
                    log.product_price = item.product_price
                    log.product_price_with_fee = item.product_price
                else:
                    log.product_price = item.product_price
                    log.product_price_with_fee = item.product_price_with_fee

            log.save()

            item.product_Quantity -= 1
            if int(item.product_Quantity) <= 0:
                item.delete()
            else:
                item.save()

            items = Item.objects.all()
            context = {"items": items, "SELF_FEE": SELF_FEE, "OK": "OK"}
            return render_with_userdata(request, self.template_name, context=context)
        except:
            # items = Item.objects.all()
            # context = {"items": items, "SELF_FEE": SELF_FEE}
            # return render_with_userdata(request, "jihanki/buy.html", context=context)
            return redirect("jihanki:buy")


class SellView(View):

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect("xdmain:login")
        else:
            itemlen = len(Item.objects.all())
            loglen = len(Log.objects.all())
            context = {"itemlen": itemlen, "loglen": loglen}
            return render_with_userdata(request, "jihanki/sell.html", context)

    def post(self, request):
        product = request.POST["product"]
        price = int(request.POST["price"])
        price_with_fee = int(float(price) * DFEE)
        # price_with_fee = math.ceil(float(price) * DFEE)
        qt = int(request.POST["qt"])

        postitemlen = int(request.POST["itemlen"])
        postloglen = int(request.POST["loglen"])
        itemlen = len(Item.objects.all())
        loglen = len(Log.objects.all())

        if (itemlen != postitemlen or loglen != postloglen):
            return render_with_userdata(request, self.template_name)

        if qt < 1 or (price < 0 and price > 3000):
            return redirect("jihanki:sell")

        for _ in range(qt):
            item = Item()
            item.product_name = product
            item.product_price = price
            item.product_price_with_fee = price_with_fee
            item.product_Quantity = 1
            item.seller = request.user
            item.added_date = datetime.datetime.now()
            item.save()

        return render_with_userdata(request, "jihanki/sell.html", {"product": product, "price": price, "OK": "OK"})


def log(request):
    logs = list(Log.objects.all())
    logs_without_payment = []

    total_price = 0
    total_price_with_fee = 0

    logs.reverse()
    for log in logs:
        if log.product_price == 0.0:
            log.delete()
            continue
        if log.product_name != "In" and log.product_name != "Out":
            total_price += log.product_price
            total_price_with_fee += log.product_price_with_fee
            logs_without_payment.append(log)
    d_fee = total_price_with_fee - total_price

    if request.user.is_staff:
        return render_with_userdata(request, "jihanki/log.html", context={"d_fee": d_fee, "logs": logs})
    else:
        return render_with_userdata(request, "jihanki/log.html", context={"d_fee": d_fee, "logs": logs_without_payment})


def news(request):
    return render_with_userdata(request, "jihanki/news.html", context={"News": News.objects.all()})


class PaymentInView(View):
    template_name = "jihanki/payment_in.html"

    def get(self, request):
        itemlen = len(Item.objects.all())
        loglen = len(Log.objects.all())
        context = {"itemlen": itemlen, "loglen": loglen}
        return render_with_userdata(request, self.template_name, context=context)

    def post(self, request):
        price = int(request.POST["price"])

        postitemlen = int(request.POST["itemlen"])
        postloglen = int(request.POST["loglen"])
        itemlen = len(Item.objects.all())
        loglen = len(Log.objects.all())

        if (itemlen != postitemlen or loglen != postloglen):
            return render_with_userdata(request, self.template_name)

        if price < 1:
            return render_with_userdata(request, self.template_name)

        log = Log()
        log.product_name = "In"
        log.product_price = price
        log.product_price_with_fee = price
        log.seller = request.user
        log.buyer = User.objects.get(username="BANK")
        log.added_date = datetime.datetime.now()
        log.deleted_date = datetime.datetime.now()
        log.save()

        return render_with_userdata(request, self.template_name, {"OK": True})


class UserAndMoneyManagement():
    username = ""
    money = 0
    pk = None


class DevView(View):
    template_name = "jihanki/dev.html"

    def get(self, request):
        if not request.user.is_staff:
            return redirect("jihanki:log")
        else:
            logs = Log.objects.all()
            total_price = 0
            total_price_with_fee = 0

            for log in logs:
                if log.product_name != "In" and log.product_name != "Out":
                    total_price += log.product_price
                    total_price_with_fee += log.product_price_with_fee
            d_fee = total_price_with_fee - total_price

            users_money = []

            for user in User.objects.all():
                uamm = UserAndMoneyManagement()
                uamm.username = user.username
                uamm.pk = user.pk
                for log in logs:
                    if log.seller == user:
                        uamm.money += log.product_price
                    if log.buyer == user:
                        uamm.money -= log.product_price_with_fee
                if ADD_ITEM_TO_BALANCE:
                    for item in Item.objects.all():
                        if item.seller == user:
                            uamm.money += item.product_price * item.product_Quantity
                users_money.append(uamm)

            item_money = 0
            for item in Item.objects.all():
                item_money += item.product_price * item.product_Quantity

            users_money.sort(key=lambda x: x.money, reverse=True)

            return render_with_userdata(request, self.template_name, {"users_money": users_money, "d_fee": d_fee, "item_money": item_money})


class PaymentOutView(View):
    template_name = "jihanki/payment_out.html"

    def get(self, request, pk):
        # return redirect("jihanki:log")

        if not request.user.is_staff:
            return redirect("jihanki:log")
        else:
            user = User.objects.get(pk=pk)
            logs = Log.objects.all()
            balance = 0

            itemlen = len(Item.objects.all())
            loglen = len(Log.objects.all())

            for log in logs:
                if log.seller == user:
                    balance += log.product_price
                if log.buyer == user:
                    balance -= log.product_price_with_fee

            return render_with_userdata(request, self.template_name, context={"balance": balance, "user": user, "itemlen": itemlen, "loglen": loglen})

    def post(self, request, pk):
        if not request.user.is_staff:
            return redirect("jihanki:log")
        else:
            user = User.objects.get(pk=pk)
            logs = Log.objects.all()
            balance = 0

            postitemlen = int(request.POST["itemlen"])
            postloglen = int(request.POST["loglen"])
            itemlen = len(Item.objects.all())
            loglen = len(Log.objects.all())

            if (itemlen != postitemlen or loglen != postloglen):
                return render_with_userdata(request, self.template_name)

            for log in logs:
                if log.seller == user:
                    balance += log.product_price
                if log.buyer == user:
                    balance -= log.product_price_with_fee

            price = int(request.POST["price"])
            if price <= balance:
                log = Log()
                log.product_name = "Out"
                log.seller = User.objects.get(username="BANK")
                log.buyer = user
                log.product_price = price
                log.product_price_with_fee = price
                log.added_date = datetime.datetime.now()
                log.deleted_date = datetime.datetime.now()
                log.save()
                return render_with_userdata(request, self.template_name, context={"balance": balance-price, "user": user, "OK": "OK"})

            return render_with_userdata(request, self.template_name, context={"balance": balance, "user": user})


class DeleteItemView(View):
    template_name = "jihanki/deleteItem.html"

    def get(self, request, pk):
        return redirect("jihanki:index")

    def post(self, request, pk):
        item = get_object_or_404(Item, pk=pk)
        if item.seller == request.user:
            item.delete()
        return redirect("jihanki:buy")


# *********** REST API **************************************

class ItemList(APIView):

    def get(self, request):
        items = Item.objects.all()
        serial = ItemSerializer(items, many=True)
        return Response(serial.data)

    def post(self):
        pass
