from django.conf.urls import url
from . import views

from rest_framework.urlpatterns import format_suffix_patterns

app_name = "jihanki"

urlpatterns = [
    # /
    # /jihanki/*
    url(r'^$',views.index,name="index"),
    url(r'^index/$',views.index,name="subindex"),
    
    url(r'^sell/$',views.SellView.as_view(),name="sellview"),

    # url(r'^sell/post/$',views.sellpost,name="sellpost"),
    # /jihanki/sell
    # url(r'^sell/priv/$',views.sell,name="sell"),

    # /jihanki/buy/post/<product_id>/
    url(r'^buyconfirm/(?P<pk>[0-9]+)/$',views.BuyConfirmView.as_view(),name="buyconfirm"),
    # /jihanki/buy
    url(r'^buy/$',views.BuyView.as_view(),name="buy"),

    url(r'^log/$',views.log,name="log"),

    url(r'^news/$',views.news,name="news"),

    url(r'^payment/in/$',views.PaymentInView.as_view(),name="paymentIn"),

    url(r'^dev/$',views.DevView.as_view(),name="dev"),

    url(r'^payment/out/(?P<pk>[0-9]+)/$',views.PaymentOutView.as_view(),name="paymentOut"),

    url(r'^deleteitem/(?P<pk>[0-9]+)/$',views.DeleteItemView.as_view(),name="deleteItem"),

    url(r'^itemlist/$',views.ItemList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)