from rest_framework import serializers
from .models import Item,Log

class ItemSerializer(serializers.ModelSerializer):

	class Meta:
		model = Item
		# fields = ("product_name","product_price","product_price_with_fee")
		fields = "__all__"

