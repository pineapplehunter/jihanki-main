from django.contrib import admin
from .models import Item,Log,News

# Register your models here.

admin.site.register(Item)
admin.site.register(Log)
admin.site.register(News)